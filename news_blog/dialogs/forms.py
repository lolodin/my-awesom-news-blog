from django import forms
from .models import Message
from ckeditor.fields import RichTextFormField


class MessageForm(forms.ModelForm):
    """Форма для сообщения"""

    class Meta:
        model = Message
        fields = ('message',)
        widgets = {
            'message': forms.Textarea(attrs={'class': 'massage__textarea', 'placeholder': "Привет!"}),
        }
