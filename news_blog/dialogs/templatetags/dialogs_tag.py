from django import template
from dialogs.models import Chat


register = template.Library()


@register.simple_tag
def get_companion(user, chat):
    """Передаём профиль собеседника"""
    for u in chat.members.all():
        if u != user:
            return u
    return None


@register.simple_tag
def get_new_messages(user, chat):
    """Передаём колличество новых сообщений"""
    new_messages = chat.message.filter(is_readed=False).exclude(author=user).count()
    return new_messages


@register.simple_tag
def get_all_new_messages(user):
    """Передаём колличество новых сообщений из всех чатов"""
    chats = Chat.objects.filter(members__in=[user])
    counter = 0
    for chat in chats:
        counter += chat.message.filter(is_readed=False).exclude(author=user).count()
    return counter
