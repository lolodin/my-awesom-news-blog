from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Count
from django.shortcuts import redirect
from django.views.generic import ListView, DetailView, CreateView
from django.views.generic.base import View

from profiles.models import Profile
from .models import Chat, Message
from .forms import MessageForm


class DialogsListView(LoginRequiredMixin, ListView):
    model = Chat
    context_object_name = 'dialog_list'
    template_name = 'dialogs/dialogs_list.html'

    def get_queryset(self):
        chats = Chat.objects.filter(members__in=[self.request.user.id])
        return chats


class DialogsDetailView(LoginRequiredMixin, DetailView):
    model = Chat
    context_object_name = 'detail_dialog'
    template_name = 'dialogs/dialogs_detail.html'

    def dispatch(self, request, *args, **kwargs):
        if self.request.user not in self.get_object().members.all():
            return redirect('/')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.check_message()
        context['dialog_list'] = Chat.objects.filter(members__in=[self.request.user.id])
        context['message_form'] = MessageForm
        return context

    def check_message(self):
        chats = self.get_object()
        chats.message.filter(is_readed=False).exclude(author=self.request.user).update(is_readed=True)
        return chats


class CreateDialogView(LoginRequiredMixin, View):

    def get(self, request, pk):
        profile = Profile.objects.get(pk=pk)
        chat = Chat.objects.filter(members__in=[request.user.id, profile.user.id]).annotate(c=Count('members')).\
            filter(c=2)
        if chat.count() == 0:
            chat = Chat.objects.create()
            chat.members.add(request.user)
            chat.members.add(profile.user)
        else:
            chat = chat.first()
        return redirect('dialogs_detail', pk=chat.pk)


class SendMessageView(LoginRequiredMixin, CreateView):
    model = Message
    form_class = MessageForm

    def form_valid(self, form):
        chat_pk = self.kwargs['pk']
        chat = Chat(pk=chat_pk)
        form.instance.chat = chat
        form.instance.author = self.request.user
        form.save()
        return redirect('dialogs_detail', chat_pk)

    def get_success_url(self):
        return redirect('dialogs_detail', self.kwargs['pk'])
