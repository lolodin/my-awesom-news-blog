from django.urls import path
from .views import DialogsListView, DialogsDetailView, CreateDialogView, SendMessageView

urlpatterns = [
    path('', DialogsListView.as_view(), name='dialogs_list'),
    path('<int:pk>/', DialogsDetailView.as_view(), name='dialogs_detail'),
    path('get_dialog/<int:pk>/', CreateDialogView.as_view(), name='get_dialogs'),
    path('send_message/<int:pk>/', SendMessageView.as_view(), name='send_message'),
]
