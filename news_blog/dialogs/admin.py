from django.contrib import admin
from .models import Chat, Message


@admin.register(Chat)
class ChatAdmin(admin.ModelAdmin):
    list_display = ('pk',)


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('pk', 'chat', 'author', 'pub_date', 'is_readed',)
    list_filter = ('chat', 'author', 'pub_date', 'is_readed',)
