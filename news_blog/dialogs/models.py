from django.db import models
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField


class Chat(models.Model):
    members = models.ManyToManyField(User, verbose_name="Участник", related_name='members')

    def __str__(self):
        return f'Чат {self.pk}'


class Message(models.Model):
    chat = models.ForeignKey(Chat, verbose_name="Чат", on_delete=models.PROTECT, related_name='message')
    author = models.ForeignKey(User, verbose_name="Пользователь", on_delete=models.PROTECT)
    message = RichTextField("Сообщение")
    pub_date = models.DateTimeField('Дата сообщения', auto_now_add=True)
    is_readed = models.BooleanField('Прочитано', default=False)

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        ordering = ['pub_date']

    def __str__(self):
        return self.message
