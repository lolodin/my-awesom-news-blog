import os

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'news_blog.settings')

app = Celery('news_blog')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'send-new-feedback': {
        'task': 'contact_us.tasks.send_new_feedback',
        'schedule': crontab(minute='*/15'),
    },
    'feedback-statistic': {
        'task': 'contact_us.tasks.feedback_statistic',
        'schedule': crontab(hour='*/1'),
    },
    'send-feedback-statistic': {
        'task': 'contact_us.tasks.send_feedback_statistic',
        'schedule': crontab(minute=0, hour=12),
    },
    'send-answer-feedback': {
        'task': 'contact_us.tasks.send_answer_feedback',
        'schedule': crontab(minute=0, hour=12),
    },
    'clean-webp': {
        'task': 'news.tasks.clean_webp',
        'schedule': crontab(day_of_week='sunday', minute=0, hour=12),
    },
    'clean-useless-img-folder': {
        'task': 'news.tasks.clean_useless_img_folder',
        'schedule': crontab(minute=0, hour=12),
    },
    'clean-webp-avatars': {
        'task': 'profiles.tasks.clean_webp_avatars',
        'schedule': crontab(day_of_week='sunday', minute=0, hour=12),
    },
}
