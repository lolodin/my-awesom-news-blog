from django.shortcuts import render


def error_400(request, exception=None):
    return render(request, 'custom_error/400.html')


def error_401(request, exception=None):
    return render(request, 'custom_error/401.html')


def error_403(request, exception=None):
    return render(request, 'custom_error/403.html')


def error_404(request, exception):
    return render(request, 'custom_error/404.html')


def error_500(request, exception=None):
    return render(request, 'custom_error/500.html')


def error_502(request, exception=None):
    return render(request, 'custom_error/502.html')


def error_503(request, exception=None):
    return render(request, 'custom_error/503.html')
