from django.apps import AppConfig


class CustomErrorConfig(AppConfig):
    name = 'custom_error'
