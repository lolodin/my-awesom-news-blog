import os
import shutil

from PIL import Image


def convert_to_webp(original_img, webp_img):
    """Конвертация исходных файлов в webp"""

    try:
        # for file in os.listdir(path_from):
        image = Image.open(original_img)
        image = image.convert('RGB')
        image.save(webp_img, 'webp')
        return webp_img
    except (ValueError, FileNotFoundError, NotADirectoryError):
        return ValueError(f'Директория {original_img=} не найдена.')


def resize_img(source_img, result_img, res_width, expansion):
    try:
        image = Image.open(source_img)
        width_percent = (res_width / float(image.size[0]))
        height_size = int(float(image.size[1]) * float(width_percent))
        image = image.resize((res_width, height_size), Image.LANCZOS)
        image.save(result_img, expansion)
    except (ValueError, FileNotFoundError):
        return ValueError(f'Директория {source_img=} не найдена.')


def clean_dir(dir_path, dir_name=None, del_all=False):
    """Очищаем или удаляем директорию"""
    if dir_name:
        try:
            current_webp_dir_path = os.path.join(dir_path, dir_name)
            shutil.rmtree(current_webp_dir_path)
        except (ValueError, FileNotFoundError):
            raise ValueError(f'Не верно указано имя директории для удаления {dir_name=}')

    elif del_all:
        try:
            all_dir = os.listdir(dir_path)
            for _dir in all_dir:
                current_dir_path = os.path.join(dir_path, _dir)
                shutil.rmtree(current_dir_path)
        except (ValueError, FileNotFoundError):
            raise ValueError(f'Не верно указан путь к деректории {dir_path=}')

    else:
        return f'Вы не указали имя конкретную директорию {dir_name=} или не дали разрешения удалить всё {del_all=}'


def get_file_name(path):
    file_path = path.split('/')
    file = file_path[-1]
    file_name = file.split('.')
    file_name = file_name[0]
    return file, file_name
