from django import forms
from django.contrib import admin
from .models import News, Comment, Category
from mptt.admin import MPTTModelAdmin
from ckeditor_uploader.widgets import CKEditorUploadingWidget


class NewsAdminForm(forms.ModelForm):
    description = forms.CharField(label='Описание', widget=CKEditorUploadingWidget())

    class Meta:
        model = News
        fields = '__all__'


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    """Новости"""
    list_display = ('id', 'author', 'title', 'like')
    list_filter = ('author', 'category')
    search_fields = ('title',)
    save_on_top = True
    form = NewsAdminForm


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    """Категории"""
    list_display = ('name', 'url')
    list_filter = ('name',)


admin.site.register(Comment, MPTTModelAdmin)