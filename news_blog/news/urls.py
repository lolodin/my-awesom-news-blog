from django.urls import path
from .views import NewsView, NewsDetail, AddComment, LikeView, AddNewsView, SearchView, FilterCategoryNewsView, \
    MainFilterNewsView, NewsEditView


urlpatterns = [
    path('', NewsView.as_view(), name='news_list'),
    path('search/', SearchView.as_view(), name='search'),
    path('filter/<str:slug>/', MainFilterNewsView.as_view(), name='filter'),
    path('filter_category/', FilterCategoryNewsView.as_view(), name='filter_category'),
    path('<int:pk>/', NewsDetail.as_view(), name='news_detail'),
    path('<int:pk>/edit/', NewsEditView.as_view(), name='news_edit'),
    path('review/<int:pk>/', AddComment.as_view(), name='add_comment'),
    path('like/', LikeView.as_view(), name='add_like'),
    path('add_news/', AddNewsView.as_view(), name='add_news'),
]