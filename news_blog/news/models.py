from django.db import models
from django.contrib.auth.models import User
from mptt.models import MPTTModel, TreeForeignKey
from ckeditor.fields import RichTextField
from .service import get_path_upload_news_image


class Category(models.Model):
    name = models.CharField('Название', max_length=100)
    description = models.TextField('Описание')
    url = models.SlugField(max_length=160, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class News(models.Model):
    """Новость"""
    title = models.CharField('Заголовок', max_length=150)
    description = RichTextField('Описание', blank=True, null=True)
    created_at = models.DateTimeField('Время публикации', auto_now_add=True)
    updated_at = models.DateTimeField('Время обновления публикации', auto_now=True)
    image = models.ImageField('Постер', upload_to=get_path_upload_news_image, blank=True, null=True)
    author = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.CASCADE, related_name="news")
    like = models.IntegerField('Like', default=0)
    user_like = models.ManyToManyField(User, verbose_name="Кто поставил like", related_name="users_like_news")
    views = models.IntegerField('Views', default=0)
    # url = models.SlugField(max_length=100, unique=True)
    category = models.ForeignKey(Category, verbose_name='Категория', on_delete=models.PROTECT, default=None, null=True)
    favourites = models.ManyToManyField(User, related_name='favourite', default=None, blank=True)

    def __str__(self):
        return self.title

    def get_comment(self):
        return self.comment_set.all()

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
        ordering = ['-created_at']


class Comment(MPTTModel):
    """Комментарий"""
    author = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.CASCADE, related_name="comment")
    text = models.TextField('Сообщение', max_length=5000)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    news = models.ForeignKey(News, verbose_name='Новости', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    like = models.IntegerField(default=0, verbose_name='Like')
    user_like = models.ManyToManyField(User, verbose_name="Кто поставил like", related_name="users_like_comment")

    class MPTTMeta:
        order_insertion_by = ['-created_at']

    def __str__(self):
        return f'{self.author}'


