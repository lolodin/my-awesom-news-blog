from django import template
from news.models import Category, News
from news.service import get_news_webp_img
from utils import get_file_name

register = template.Library()


@register.inclusion_tag('news/tags/item_pagination.html')
def get_item_pages(item):
    """Передаём имя page_obj"""
    return {'paginator': item}


@register.simple_tag()
def get_categories():
    """Получаем все категории"""
    return Category.objects.all()


@register.simple_tag()
def get_webp_img(news_id):
    """Отдаём webp image(cover)"""
    # get current image(cover) name
    news = News.objects.get(pk=news_id)
    news_img_path = news.image.url
    file, file_name = get_file_name(path=news_img_path)

    webp_x2_path, webp_x1_path = get_news_webp_img(news_id=news_id,
                                                   img=file,
                                                   img_name=file_name
                                                   )

    data = {
        'webp_x2': webp_x2_path,
        'webp_x1': webp_x1_path,
    }
    return data


