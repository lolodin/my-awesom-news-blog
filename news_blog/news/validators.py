from copy import copy

from PIL import Image
from django import forms

from news.service import remove_html_tag


def news_description_validation(origin_description):
    MIN_DESCRIPTION_CHARACTERS = 200
    MAX_DESCRIPTION_CHARACTERS = 2500

    description = copy(origin_description)
    if not description:
        message = 'Поле обязательное к заполнению'
        raise forms.ValidationError(message)

    description_clean = remove_html_tag(value=description)
    if len(description_clean) < MIN_DESCRIPTION_CHARACTERS:
        message = f'Минимальное колличество символов ({MIN_DESCRIPTION_CHARACTERS})'
        raise forms.ValidationError(message)
    if len(description_clean) > MAX_DESCRIPTION_CHARACTERS:
        message = f'Максимальное колличество символов ({MAX_DESCRIPTION_CHARACTERS})'
        raise forms.ValidationError(message)


def news_image_validation(image):
    MIN_RESOLUTION = 650, 300
    MAX_RESOLUTION = 1920, 1080
    MAX_UPLOAD_SIZE = 5242880

    if image.size > MAX_UPLOAD_SIZE:
        message = 'Максимальый размер изображения 5 МБ'
        raise forms.ValidationError(message)

    img = Image.open(image)
    min_width, min_height = MIN_RESOLUTION
    max_width, max_height = MAX_RESOLUTION

    if img.width < min_width or img.height < min_height:
        message = f'Разрешение изображения меньше минимального ' \
                  f'({min_width}x{min_height})'
        raise forms.ValidationError(message)
    if img.width > max_width or img.height > max_height:
        message = f'Разрешение изображения больше максимального ' \
                  f'({max_width}x{max_height})'
        raise forms.ValidationError(message)


def news_category_validation(category):
    if not category:
        message = 'Выберите категорию'
        raise forms.ValidationError(message)