import os
import re
import shutil
import time

from news_blog import settings
from utils import convert_to_webp, resize_img, clean_dir

NEWS_IMG_MEDIA_PATH = os.path.join(settings.BASE_DIR, 'media')
NEWS_IMG_ORIGINAL_PATH = os.path.join(NEWS_IMG_MEDIA_PATH, 'news')
NEWS_IMG_STATIC_PATH = os.path.join(settings.BASE_DIR, 'static')
NEWS_IMG_WEBP_SHORT_PATH = os.path.join('img', 'news_webp')
NEWS_IMG_WEBP_FULL_PATH = os.path.join(NEWS_IMG_STATIC_PATH, NEWS_IMG_WEBP_SHORT_PATH)


def _get_path_for_news_image(news_id, file, file_name):
    # get path news original img
    current_image_dict = os.path.join(NEWS_IMG_ORIGINAL_PATH, f'{news_id}')  # ../media/news/1
    current_original_img = os.path.join(current_image_dict, file)  # /media/news/1/news_1

    # get path news webp img
    current_image_webp_dir = os.path.join(NEWS_IMG_WEBP_FULL_PATH, f'{news_id}')
    webp_x2_full_path = os.path.join(NEWS_IMG_WEBP_FULL_PATH, f'{news_id}', f'{file_name}x2.webp')
    webp_x1_full_path = os.path.join(NEWS_IMG_WEBP_FULL_PATH, f'{news_id}', f'{file_name}x1.webp')
    webp_x2 = os.path.join(NEWS_IMG_WEBP_SHORT_PATH, f'{news_id}', f'{file_name}x2.webp')
    webp_x1 = os.path.join(NEWS_IMG_WEBP_SHORT_PATH, f'{news_id}', f'{file_name}x1.webp')

    data = {
        'original_img': current_original_img,
        'webp_dir': current_image_webp_dir,
        'webp_x2_full_path': webp_x2_full_path,
        'webp_x1_full_path': webp_x1_full_path,
        'webp_x2': webp_x2,
        'webp_x1': webp_x1,
    }
    return data


def _check_webp_exist(webp_x2, webp_x1):
    webp_x2_path = os.path.join(NEWS_IMG_STATIC_PATH, webp_x2)
    webp_x1_path = os.path.join(NEWS_IMG_STATIC_PATH, webp_x1)
    if os.path.exists(webp_x2_path) and os.path.exists(webp_x1_path):
        return True
    return False


def get_news_webp_img(news_id, img, img_name):
    """Конвертируем обложку новости в webp"""

    path = _get_path_for_news_image(news_id=news_id, file=img, file_name=img_name)

    # check current webp dir exist
    if not os.path.isdir(path['webp_dir']):
        os.makedirs(path['webp_dir'])

    if _check_webp_exist(webp_x2=path['webp_x2'], webp_x1=path['webp_x1']) is False:

        # (media) /news/1/news_1 -> (static) img/news_webp/1/news_1.webp
        convert_to_webp(original_img=path['original_img'],
                        webp_img=path['webp_x2_full_path']
                        )

        # (static) img/news_webp/1/news_1x2.webp -> (static) img/news_webp/1/news_1x1.webp
        resize_img(source_img=path['webp_x2_full_path'],
                   result_img=path['webp_x1_full_path'],
                   res_width=650,
                   expansion='webp')

    return path['webp_x2'], path['webp_x1']


def get_path_upload_news_image(instance, file):
    """
    Редактирует путь к обложке новости
    Формат вывода: (media)/news/1/news_1
    """
    if instance.id is None:
        print(instance.author)
        instance.id = int(time.time())

    news_image_current_path = os.path.join(NEWS_IMG_ORIGINAL_PATH, f'{instance.id}')
    if os.path.isdir(news_image_current_path):
        clean_dir(dir_path=NEWS_IMG_ORIGINAL_PATH, dir_name=f'{instance.id}')
        clean_dir(dir_path=NEWS_IMG_WEBP_FULL_PATH, dir_name=f'{instance.id}')

    end_extension = file.split('.')[-1]
    head = f'news_{instance.id}'
    file_name = f'{head}.{end_extension}'

    return os.path.join('news', f'{instance.id}', f'{file_name}')


def send_news_id_to_image_path(form, request):
    form.instance.image = None
    form.save()

    # Для передачи id в image path
    form.instance.image = request.FILES["image"]


def remove_html_tag(value):
    clean_value = re.sub(r'<.*?>', '', value)
    clean_value = clean_value.replace('&nbsp;', '')
    clean_value = clean_value.strip()
    return clean_value


def remove_useless_news_img(news_id):
    news_dir = os.listdir(NEWS_IMG_ORIGINAL_PATH)

    for _id in news_id:
        _id = str(_id)
        if _id in news_dir:
            news_dir.remove(_id)

    for elem in news_dir:
        path = os.path.join(NEWS_IMG_ORIGINAL_PATH, elem)

        if os.path.exists(path):
            try:
                if os.path.isdir(path):
                    shutil.rmtree(path)
                else:
                    os.remove(path)
            except (ValueError, TypeError):
                message = f'Что-то пошло не так {path=}'
                raise ValueError(message)