from news_blog.celery import app

from .models import News
from .service import clean_dir, NEWS_IMG_WEBP_FULL_PATH, remove_useless_news_img


@app.task
def clean_webp():
    clean_dir(dir_path=NEWS_IMG_WEBP_FULL_PATH, del_all=True)


@app.task
def clean_useless_img_folder():
    news = News.objects.exclude(image='')
    news_id = news.values_list('id', flat=True)

    remove_useless_news_img(news_id=news_id)
