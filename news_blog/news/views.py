from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.db.models import Q, Count
from django.http import JsonResponse, Http404
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView
from django.views.generic.base import View
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import News, Comment, Category
from .forms import CommentForm, NewsForm
from .service import send_news_id_to_image_path


class NewsView(ListView):
    """Список новостей"""
    model = News
    queryset = News.objects.all()
    context_object_name = 'news_list'
    paginate_by = 5


class NewsEditView(LoginRequiredMixin, UpdateView):
    model = News
    form_class = NewsForm
    template_name = 'news/news_edit.html'

    def get_context_data(self, **kwargs):
        content = super().get_context_data(**kwargs)
        content['categories'] = Category.objects.all()
        return content

    def dispatch(self, request, *args, **kwargs):
        if self.get_object().author != self.request.user:
            return redirect('/')
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('news_detail', args=(self.get_object().pk,))


class NewsDetail(DetailView):
    """Список новостей"""
    model = News

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['comment_form'] = CommentForm()
        context['all_comments'] = self.get_pages()
        context['views_counter'] = self.get_views()
        return context

    def get_views(self):
        """Добавляет просмотр к статье"""
        views_counter = self.object
        views_counter.views += 1
        views = views_counter.views
        views_counter.save()
        return views

    def get_pages(self):
        """Делит комментарии на страницы"""
        # TODO Доработать деление. Не выводить children элементы на следующую страницу
        all_comments = self.object.get_comment()
        page = self.request.GET.get('page', 1)
        paginator = Paginator(all_comments, 25)
        try:
            comments = paginator.page(page)
        except PageNotAnInteger:
            comments = paginator.page(1)
        except EmptyPage:
            comments = paginator.page(paginator.num_pages)
        return comments


class AddComment(LoginRequiredMixin, View):
    """Комментарии"""

    def get(self, request):
        raise Http404

    def post(self, request, pk):
        form = CommentForm(request.POST)
        news = News.objects.get(pk=pk)
        if form.is_valid():
            form = form.save(commit=False)
            if request.POST.get('parent', None):
                form.parent_id = int(request.POST.get('parent'))
            form.news = news
            form.author = request.user
            form.save()
        return redirect(f'/{pk}')


class LikeView(LoginRequiredMixin, View):
    """Поставить лайк"""

    def get(self, request):
        raise Http404

    def post(self, request):
        pk = request.POST.get("pk")
        model_name = request.POST.get("model")
        if model_name == 'news':
            news = News.objects.get(pk=pk)
            if request.user in news.user_like.all():
                news.user_like.remove(User.objects.get(pk=request.user.pk))
                news.like -= 1
                counter = news.like
                result = False
            else:
                news.user_like.add(User.objects.get(pk=request.user.pk))
                news.like += 1
                counter = news.like
                result = True
            news.save()
        else:
            comment = Comment.objects.get(pk=pk)
            if request.user in comment.user_like.all():
                comment.user_like.remove(User.objects.get(pk=request.user.pk))
                comment.like -= 1
                counter = comment.like
                result = False
            else:
                comment.user_like.add(User.objects.get(pk=request.user.pk))
                comment.like += 1
                counter = comment.like
                result = True
            comment.save()
        print(model_name)
        return JsonResponse({'result': result, 'counter': counter, 'model_name': model_name, })


class AddNewsView(LoginRequiredMixin, CreateView):
    model = News
    form_class = NewsForm
    template_name = 'news/add_news.html'

    def get_context_data(self, **kwargs):
        content = super().get_context_data(**kwargs)
        content['categories'] = Category.objects.all()
        return content

    def form_valid(self, form):
        form.instance.author = self.request.user

        if form.cleaned_data["image"]:
            send_news_id_to_image_path(form=form, request=self.request)

        form.save()
        return redirect('/')

    def get_success_url(self):
        return redirect('/')


class FilterCategoryNewsView(ListView):
    """Фильтр новостей по категории"""
    paginate_by = 5

    def get_queryset(self):
        queryset = News.objects.filter(
            category__in=self.request.GET.getlist('category')
        ).distinct()
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['category'] = ''.join([f'category={x}&'for x in self.request.GET.getlist('category')])
        return context


class SearchView(ListView):
    """Поиск новостей по заголовку и описанию"""
    paginate_by = 10

    def get_queryset(self):
        return News.objects.filter(
            Q(title__icontains=self.request.GET.get('q').capitalize()) |
            Q(description__icontains=self.request.GET.get('q').capitalize()),
        ).distinct()

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        context['q'] = f"q={self.request.GET.get('q')}&"
        return context


class MainFilterNewsView(ListView):
    """Фильтр новостей по лайкам и комментариям"""
    paginate_by = 10

    def get_queryset(self):
        slug = self.kwargs.get('slug')
        if slug == 'hot':
            # Сортировка статей по комментариям
            return News.objects.annotate(count=Count('comment')).order_by('-count').all()
        else:
            # Сортировка статей по лайкам
            return News.objects.order_by('-like').all()