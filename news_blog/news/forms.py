from django import forms
from .models import Comment, News
from mptt.forms import TreeNodeChoiceField
from ckeditor.fields import RichTextFormField
from .validators import news_description_validation, news_image_validation, news_category_validation


class CommentForm(forms.ModelForm):
    """Форма для отправки комментария к новости"""
    parent = TreeNodeChoiceField(queryset=Comment.objects.all())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['parent'].widget.attrs.update(
            {'class': 'hide_all'})
        self.fields['parent'].label = ''
        self.fields['text'].label = ''
        self.fields['parent'].required = False

    class Meta:
        model = Comment
        fields = ('parent', 'text',)
        widgets = {
            'text': forms.Textarea(attrs={'class': 'form-comment__textarea', 'placeholder': "Написать комментарий..."})
        }

    def save(self, *args, **kwargs):
        Comment.objects.rebuild()
        return super(CommentForm, self).save(*args, **kwargs)


class NewsForm(forms.ModelForm):
    """Форма для добавление новости"""

    class Meta:
        model = News
        fields = ('title', 'description', 'image', 'category')
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-news__input', 'placeholder': "Заголовок..."}),
            'description': RichTextFormField(),
            'image': forms.ClearableFileInput(attrs={'class': ''}),
            'category': forms.Select(attrs={'class': 'select__select'}),
        }

    def clean_description(self):
        origin_description = self.cleaned_data['description']
        news_description_validation(origin_description)
        return origin_description

    def clean_image(self):
        image = self.cleaned_data['image']
        if image:
            news_image_validation(image=image)
            return image

    def clean_category(self):
        category = self.cleaned_data['category']
        news_category_validation(category=category)
        return category
