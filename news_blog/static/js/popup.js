// Получаем все элементы с классом 'popup-link'
const popupLinks = document.querySelectorAll(".popup-link");
// Блокировка скрола внутри 'body'
const body = document.querySelector("body");
const lockPadding = document.querySelectorAll(".lock-padding"); //for position:fixed elements

// Для того, чтобы не было двойных нажатий
let unlock = true;

// Ровна popup transition. Нужна для блокировки скрола
const timeout = 800;

// Собирвет все элементы с классом '.popup-link' и запускает popupOpen
if (popupLinks.length > 0) {
  for (let index = 0; index < popupLinks.length; index++) {
    const popupLink = popupLinks[index];
    popupLink.addEventListener("click", function (e) {
      console.log(popupLink.getAttribute("href").replace("#", ""))
      const popupName = popupLink.getAttribute("href").replace("#", "");
      const currentPopup = document.getElementById(popupName);
      popupOpen(currentPopup);
      e.preventDefault(); //запрещает перезагружать страницу
    });
  }
}
// Для объектов с классом 'close-popup', которые будут закрывать popup
const popupCloseIcon = document.querySelectorAll(".close-popup");
if (popupCloseIcon.length > 0) {
  for (let index = 0; index < popupCloseIcon.length; index++) {
    const el = popupCloseIcon[index];
    el.addEventListener("click", function (e) {
      popupClose(el.closest(".popup")); // Ищет ближайшего родителя с классом 'popup'
      e.preventDefault(); //запрещает перезагружать страницу
    });
  }
}

function popupOpen(currentPopup) {
  if (currentPopup && unlock) {
    const popupActive = document.querySelector(".popup.open");
    if (popupActive) {
      popupClose(popupActive, false);
    } else {
      bodyLock();
    }
    currentPopup.classList.add("open");
    currentPopup.addEventListener("click", function (e) {
      if (!e.target.closest(".popup__content")) {
        popupClose(e.target.closest(".popup"));
      }
    });
  }
}

// Нужно ли уберать скрол при открытии в popup другого popup
function popupClose(popupActive, doUnclock = true) {
  if (unlock) {
    popupActive.classList.remove("open");
    if (doUnclock) {
      bodyUnlock();
    }
  }
}

// Замараживаем скролл body на большинстве браузеров при открытии popup
function bodyLock() {
  const lockPaddingValue =
    window.innerWidth - document.querySelector(".wrapper").offsetWidth + "px"; //ширина скрола
  if (lockPadding.length > 0) {
    for (let index = 0; index < lockPadding.length; index++) {
      const el = lockPadding[index];
      el.getElementsByClassName.paddingRight = lockPaddingValue;
    }
  }
  body.style.paddingRight = lockPaddingValue; //body width без скрола
  body.classList.add("lock");

  unlock = false;
  setTimeout(function () {
    unlock = true;
  }, timeout);
}

// Появление скрола только после оканчания закрытия popup анимации
function bodyUnlock() {
  setTimeout(function () {
    if (lockPadding.length > 0) {
      for (let index = 0; index < lockPadding.length; index++) {
        const el = lockPadding[index];
        el.style.paddingRight = "0px";
      }
    }
    body.style.paddingRight = "0px";
    body.classList.remove("lock");
  }, timeout);

  unlock = false;
  setTimeout(function () {
    unlock = true;
  }, timeout);
}

// Закрытие popup при нажатии Esc
document.addEventListener("keydown", function (e) {
  if (e.which === 27) {
    const popupActive = document.querySelector(".popup.open");
    popupClose(popupActive);
  }
});

//Polyfills для поддержки в старых браузерах
(function () {
  if (!Element.prototype.closest) {
    Element.prototype.closest = function (css) {
      var node = this;
      while (node) {
        if (node.matches(css)) return node;
        else node = node.parentElement;
      }
      return null;
    };
  }
})();
(function () {
  if (!Element.prototype.matches) {
    Element.prototype.matches =
      Element.prototype.matchesSelector ||
      Element.prototype.webkitMatchesSelector ||
      Element.prototype.mozMatchesSelector ||
      Element.prototype.msMatchesSelector;
  }
})();