// Установка csrf_token
(function () {
    let csrftoken = Cookies.get('csrftoken');
    $.ajaxSetup({
        headers: {"X-CSRFToken": csrftoken}
    });
})();

//Показать/скрыть форму ответа на комментарий
var openForm = function(id) {
    $(`#${id}`).toggle()
};

//Срабатывание на фейковую кнопку загрузки аватара
var uploadAvatar = function() {
    $('#id_avatar').click();
};

//Забираем имя загружаемого файла
$('#id_avatar').on('change', function() {
  var splittedFakePath = this.value.split('\\');
  $('#file_name').text(splittedFakePath[splittedFakePath.length - 1]);
});

//Срабатывание на фейковую кнопку загрузки обложки
var uploadImage = function() {
    $('#id_image').click();
};

//Забираем имя загружаемого файла
$('#id_image').on('change', function() {
  var splittedFakePath = this.value.split('\\');
  $('#file_name').text(splittedFakePath[splittedFakePath.length - 1]);
});

//Удалить сообщение при нажатии
$('.btn_alert').on('click', function (){
  $(this).remove();
});

// Добавить закладку

const protocol = document.location.protocol
const host = document.location.host

let favorite = function (id) {
    $.ajax({
        url: protocol + "//" + host + "/profiles/add_favorite/",
        type: "POST",
        data: {
            pk: id,
        },
        success: function (json) {
            $(`#counter__favorite${id}`).text(json['counter'])
            if(json['result'] == true) {
                $(`#favorite_btn${id}`).addClass('_active')
            }
            if(json['result'] == false) {
                $(`#favorite_btn${id}`).removeClass('_active')
            };
        },
        error: function (xhr, errmsg, err) {

        }
    })
};

// Поставить лайк новости или комментарию
let like = function (id, model_name) {
    $.ajax({
        url: protocol + "//" + host + "/like/",
        type: "POST",
        data: {
            pk: id,
            model: model_name,
        },
        success: function (json) {
            if(json['model_name'] == 'news') {
                $(`#counter__likes${id}`).text(json['counter'])
                if(json['result'] == true) {
                    $(`#like_btn${id}`).addClass('_active');
                }
                if(json['result'] == false) {
                    $(`#like_btn${id}`).removeClass('_active');
                };
            }
            else {
                $(`#comment_counter__likes${id}`).text(json['counter'])
                if(json['result'] == true) {
                    $(`#comment_like_btn${id}`).addClass('_active');
                }
                if(json['result'] == false) {
                    $(`#comment_like_btn${id}`).removeClass('_active');
                }
            }
        },
        error: function (xhr, errmsg, err) {

        }
    })
};

// Подписаться на пользователя
let follow = function (pk) {
    $.ajax({
        url: protocol + "//" + host + "/profiles/add_follow/",
        type: "POST",
        data: {
            pk: pk,
        },
        success: function (json) {
            if(json['result'] == true) {
                $(`#follow${pk}`).addClass('hide_all')
                $(`#follow2${pk}`).addClass('hide_all')
                $(`#unfollow${pk}`).removeClass('hide_all')
                $(`#unfollow2${pk}`).removeClass('hide_all')
            }
            if(json['result'] == false) {
                $(`#follow${pk}`).removeClass('hide_all')
                $(`#follow2${pk}`).removeClass('hide_all')
                $(`#unfollow${pk}`).addClass('hide_all')
                $(`#unfollow2${pk}`).addClass('hide_all')
            };
        },
        error: function (xhr, errmsg, err) {

        }
    })
};

//Подсвечивать выбранную ссылку в профиле
$(document).ready(function(){
  $('.profile-nav__item [href]').each(function(){
    if (this.href == window.location.href) {
        $(this).parent().addClass('_active');
    }
  });
});

//$('.btn_alert').on('click', function (){
//  $('#id_image').click();
//});

var openCategories = function() {
    var cat = document.getElementById("categories");
    if (cat.classList.contains("filter-box_hidden") == true) {
        cat.classList.remove("filter-box_hidden");
        cat.classList.add("form-filter__box");
    }
    else {
        cat.classList.add("filter-box_hidden");
        cat.classList.remove("form-filter__box");
    }
};

let message = function(pk) {
    $.ajax({
        url: protocol + "//" + host + "/dialogs/send_message/`${pk}`/",
        type: "POST",
        data: {
            pk: pk,
        },
        success: function (json) {
            if(json['result'] == true) {
                $(`#follow${pk}`).addClass('hide_all')
                $(`#follow2${pk}`).addClass('hide_all')
                $(`#unfollow${pk}`).removeClass('hide_all')
                $(`#unfollow2${pk}`).removeClass('hide_all')
            }
            if(json['result'] == false) {
                $(`#follow${pk}`).removeClass('hide_all')
                $(`#follow2${pk}`).removeClass('hide_all')
                $(`#unfollow${pk}`).addClass('hide_all')
                $(`#unfollow2${pk}`).addClass('hide_all')
            };
        },
        error: function (xhr, errmsg, err) {

        }
    })

};

// Sidebar icon
const iconsSidebar = document.querySelectorAll('.sidebar-icon');

if (iconsSidebar){
    const menuSidebar = document.querySelector('.section-sidebar');
    iconsSidebar.forEach(function(e) {
        e.addEventListener("click", function(event) {
            
            document.body.classList.toggle('lock'); // Блокирует прокрутку контента сайта, когда ты в меню
            menuSidebar.classList.toggle('_active'); // выезжает sidebar
            document.body.classList.toggle('_active'); // окраска заднего фона
        })
    })
};

// Dialog list icon
const iconsChats = document.querySelectorAll('.chats-btn');
if (iconsChats){
    const listChat = document.querySelector('.dialogs__list');
    iconsChats.forEach(function(e) {
        e.addEventListener("click", function(event) {
            listChat.classList.toggle('_active');
        })
    })
};

// Profile nav slider
$(document).ready(function(){
    $('.profile-nav__slider').slick({
        infinite: false,
        variableWidth: true,
        arrows: true, 
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
              },
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ]
    });
});

// Custom select
document.querySelectorAll('.select').forEach(function (selectWrapper) {
    const selectBtn = selectWrapper.querySelector('.select__button');
    const selectList = selectWrapper.querySelector('.select__list');
    const selectItems = selectList.querySelectorAll('.select__item');
    const selectSelect = selectWrapper.querySelector('.select__select');
    const selectSelectText = selectSelect.options[selectSelect.selectedIndex].text;
    const newsSelectCategory = $('#id_category')

    // Удаляем атрибут select
    newsSelectCategory.removeAttr('required');

    // Передаём текущее значение select
    selectBtn.innerText = selectSelectText; 

    // Клик по кнопке. Открыть/Закрыть select
    selectBtn.addEventListener('click', function (e) {
        selectList.classList.toggle('_visible');
        this.classList.toggle('_active');
    });

    // Выбор элемента списка. Запомнить выбранное значение. Закрыть select
    selectItems.forEach(function (item) {
        item.addEventListener('click', function (e) {
            e.stopPropagation();
            selectBtn.innerText = this.innerText;
            selectBtn.focus();
            selectSelect.value = this.dataset.value;
            selectBtn.classList.remove('_active');
            selectList.classList.remove('_visible');
        });
    });

    // Клик снаружи select. Закрыть select
    document.addEventListener('click', function (e) {
        if (e.target !== selectBtn) {
            selectBtn.classList.remove('_active');
            selectList.classList.remove('_visible');
        }
    });

    // Нажатие на Tab или Escape. Закрыть select
    document.addEventListener('keydown', function (e) {
        if (e.key === 'Tab' || e.key === 'Escape') {
            selectBtn.classList.remove('_active');
            selectList.classList.remove('_visible');
        }
    });
});

// Связаться с нами
function sendFeedback() {
    const feedbackFormId = '#feedback-form'

    $(feedbackFormId).on('submit', (e) => {
        e.preventDefault()

        $.ajax({
            url: protocol + "//" + host + "/contact_us/",
            type: 'POST',
            dataType: 'json',
            data: {
                name: $(`#feedback-name`).val(),
                email: $(`#feedback-email`).val(),
                comment: $(`#feedback-comment`).val(),
            },
            success: (data) => {
                // console.log(data)

                // выводим ошибки в форме
                if('errors' in data) {
                   // console.log('Has errors =(')
                   // console.log(data['errors'])

                    // приводим все поля к valid форме
                    $(feedbackFormId + ' input, ' + feedbackFormId + ' textarea').each((index, el) => {
                        console.log(el)
                        $(el).removeClass('is-invalid').addClass('is-valid')
                        $(el).prev("span").removeClass('is-invalid').addClass('is-valid')
                    })

                    // удаляем сообщения об ошибках
                    $('.contact-us__invalid-feedback').each((index, el) => {
                        $(el).remove()
                    })

                    // выводим ошибки в форме
                    for (let key in data['errors']) {
                        // console.log('key: ' + key)
                        // console.log('val: ' + data['errors'][key])

                        $(feedbackFormId).find('textarea[name="' + key + '"]').removeClass('is-valid').addClass('is-invalid')
                        $(feedbackFormId).find('textarea[name="' + key + '"]').prev("span").removeClass('is-valid').addClass('is-invalid')
                        $(feedbackFormId).find('input[name="' + key + '"]').removeClass('is-valid').addClass('is-invalid')
                        $(feedbackFormId).find('input[name="' + key + '"]').prev("span").removeClass('is-valid').addClass('is-invalid')

                        $(feedbackFormId).find('input[name="' + key + '"]').after(() => {
                            let result = ''
                            for(let k in data['errors'][key]) {
                                result += data['errors'][key][k] + '<br>'
                            }
                            return '<div class="contact-us__invalid-feedback">' + data['errors'][key] + '</div>'
                        })

                        $(feedbackFormId).find('textarea[name="' + key + '"]').after(() => {
                            let result = ''
                            for(let k in data['errors'][key]) {
                                result += data['errors'][key][k] + '<br>'
                            }
                            return '<div class="contact-us__invalid-feedback">' + data['errors'][key] + '</div>'
                        })
                   }
                }

                // очищаем форму и показываем плашку о том, что форма отправлена
                if('success' in data) {
                    // console.log('Success!!!');

                    // удаляем классы и сообщения валидации
                    $(feedbackFormId + ' input, ' + feedbackFormId + ' textarea').each((index, el) => {
                        console.log(el)
                        $(el).removeClass('is-invalid', 'is-valid')
                        $(el).prev("span").removeClass('is-invalid', 'is-valid')
                    })
                    $('.contact-us__invalid-feedback').each((index, el) => {
                        $(el).remove()
                    })
                    
                    $('#success-feedback').addClass('open')
                    $('body').addClass('lock');

                    $(feedbackFormId)[0].reset();
                }
            },
        })
    })
}

$(document).ready(() => {

    sendFeedback()
})
