from django.urls import path, include

urlpatterns = [
    path('pages/', include('django.contrib.flatpages.urls')),
]