from django.apps import AppConfig


class CustomFlatpagesConfig(AppConfig):
    name = 'custom_flatpages'
