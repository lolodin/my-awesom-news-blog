from django.contrib import admin
from django.contrib.flatpages.admin import FlatPageAdmin
from .models import *


class CustomFlatpageInline(admin.StackedInline):
    model = CustomFlatpage
    verbose_name = "Содержание"


class FlatPageCustomAdmin(FlatPageAdmin):
    inlines = [CustomFlatpageInline]
    fieldsets = (
        (None, {'fields': ('url', 'title', 'sites')}),
        (('Advanced options'), {
            'fields': ('template_name',),
        }),
    )
    list_display = ('url', 'title')
    list_filter = ('sites', 'registration_required')
    search_fields = ('url', 'title')


admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageCustomAdmin)