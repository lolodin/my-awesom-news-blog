from django.db import models
from django.contrib.flatpages.models import FlatPage
from ckeditor.fields import RichTextField


class CustomFlatpage(models.Model):
    flatpage = models.OneToOneField(FlatPage, on_delete=models.CASCADE)
    description = RichTextField('Основной текстовый контент страницы', default='')
    text_block = RichTextField('Дополнительный блок текста', default='', blank=True)

    def __str__(self):
        return self.flatpage.title

    class Meta:
        verbose_name = "Содержание страницы"
        verbose_name_plural = "Содержание страницы"
