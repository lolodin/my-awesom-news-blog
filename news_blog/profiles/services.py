import os
from datetime import datetime

from news_blog import settings
from utils import convert_to_webp

AVATAR_MEDIA_PATH = os.path.join(settings.BASE_DIR, 'media')
AVATAR_ORIGINAL_PATH = os.path.join(AVATAR_MEDIA_PATH, 'profile_pics')
AVATAR_STATIC_PATH = os.path.join(settings.BASE_DIR, 'static')
AVATAR_WEBP_SHORT_PATH = os.path.join('img', 'profile_webp')
AVATAR_WEBP_FULL_PATH = os.path.join(AVATAR_STATIC_PATH, AVATAR_WEBP_SHORT_PATH)


def _get_path_for_avatar(user_id, file, file_name):
    # get path avatar original
    current_avatar_dict = os.path.join(AVATAR_ORIGINAL_PATH, f'user_{user_id}')  # ../media/profile_pics/user_[1]
    # /media/profile_pics/user_1/something[_2021-06-14] ->
    current_original_avatar = os.path.join(current_avatar_dict, file)

    # get path avatar webp
    current_avatar_webp_dir = os.path.join(AVATAR_WEBP_FULL_PATH, f'user_{user_id}')
    webp_full_path = os.path.join(current_avatar_webp_dir, f'{file_name}.webp')
    webp = os.path.join(AVATAR_WEBP_SHORT_PATH, f'user_{user_id}', f'{file_name}.webp')

    data = {
        'original_avatar': current_original_avatar,
        'webp_dir': current_avatar_webp_dir,
        'webp_full_path': webp_full_path,
        'webp': webp,
    }
    return data


def _check_webp_exist(webp):
    if os.path.exists(webp):
        return True
    return False


def get_avatar_webp(user_id, img, img_name):
    """Конвертируем обложку новости в webp"""

    path = _get_path_for_avatar(user_id=user_id, file=img, file_name=img_name)

    # check current webp dir exist
    if not os.path.isdir(path['webp_dir']):
        os.makedirs(path['webp_dir'])

    if _check_webp_exist(webp=path['webp_full_path']) is False:

        # (media) /news/1/news_1 -> (static) img/news_webp/1/news_1.webp
        convert_to_webp(original_img=path['original_avatar'],
                        webp_img=path['webp_full_path']
                        )

    return path['webp']


def get_path_upload_avatar(instance, file):
    """
    Редактирует путь к аватару конкретного пользователя
    Формат вывода: (media)/profile_pics/user_1/myphoto_2018-12-2.png
    """
    time = datetime.now().strftime("%Y-%m-%d")
    end_extention = file.split('.')[-1]
    head = file.split('.')[0]
    if len(head) > 10:
        head = head[:10]
    file_name = head + '_' + time + '.' + end_extention
    return os.path.join('profile_pics', f'user_{instance.user.id}', f'{file_name}')

