from django.urls import path
from .views import ProfileView, ProfileEditView, AddFavouriteView, FavouriteListView, ProfileNewsView, \
    ProfileCommentsView, ProfilePublicInfo, PublicProfileNewsView, PublicProfileCommentsView, FollowProfileView, \
    ProfileFavoritesView, ProfileFollowView, ProfileSubscribersView, ProfileSubscriptionsView, PublicProfileFollowView, \
    PublicProfileSubscriptionsView, PublicProfileSubscribersView, PublicProfileFavoritesView

urlpatterns = [
    path('', ProfileView.as_view(), name='view_profile'),
    path('edit_profile/', ProfileEditView.as_view(), name='edit_profile'),
    path('public-info/<int:pk>/', ProfilePublicInfo.as_view(), name='public_info'),
    path('add_favorite/', AddFavouriteView.as_view(), name='add_favorite'),
    path('add_follow/', FollowProfileView.as_view(), name='add_follow'),
    path('favorites2/', FavouriteListView.as_view(), name='favorites'),

    path('news/', ProfileNewsView.as_view(), name='profile_news'),
    path('comments/', ProfileCommentsView.as_view(), name='profile_comments'),
    path('follow/', ProfileFollowView.as_view(), name='profile_follow'),
    path('subscribers/', ProfileSubscribersView.as_view(), name='profile_subscribers'),
    path('subscriptions/', ProfileSubscriptionsView.as_view(), name='profile_subscriptions'),
    path('favorites/', ProfileFavoritesView.as_view(), name='profile_favorites'),

    path('public-info/<int:pk>/news/', PublicProfileNewsView.as_view(), name='public_profile_news'),
    path('public-info/<int:pk>/comments/', PublicProfileCommentsView.as_view(), name='public_profile_comments'),
    path('public-info/<int:pk>/follow/', PublicProfileFollowView.as_view(), name='public_profile_follow'),
    path('public-info/<int:pk>/subscribers/', PublicProfileSubscribersView.as_view(),
         name='public_profile_subscribers'),
    path('public-info/<int:pk>/subscriptions/', PublicProfileSubscriptionsView.as_view(),
         name='public_profile_subscriptions'),
    path('public-info/<int:pk>/favorites/', PublicProfileFavoritesView.as_view(), name='public_profile_favorites'),
]