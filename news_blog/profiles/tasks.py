from news_blog.celery import app

from profiles.services import AVATAR_WEBP_FULL_PATH
from utils import clean_dir


@app.task
def clean_webp_avatars():
    clean_dir(dir_path=AVATAR_WEBP_FULL_PATH, del_all=True)
