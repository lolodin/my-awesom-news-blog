from django.http import Http404, JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.views.generic.base import View
from news.models import News
from .models import Profile, UserFollowing
from .forms import ProfileForm
from django.views.generic import UpdateView, DetailView, ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.contrib import messages


class ProfileView(LoginRequiredMixin, DetailView):
    """Вывод профиля пользователя"""
    model = Profile
    context_object_name = 'profile'
    template_name = 'profiles/profile_detail.html'

    def get_object(self, queryset=None):
        obj = get_object_or_404(Profile, user=self.request.user)
        if obj.user != self.request.user:
            raise Http404
        return obj


class ProfileEditView(LoginRequiredMixin, UpdateView):
    """Редактирование профиля"""
    form_class = ProfileForm
    model = Profile
    template_name = 'profiles/profile_edit.html'
    success_url = reverse_lazy('edit_profile')

    def get_object(self, queryset=None):
        return self.request.user.profile

    def form_valid(self, form):
        messages.success(self.request, 'Profile has been updated!')
        return super().form_valid(form)


class ProfilePublicInfo(LoginRequiredMixin, DetailView):
    """Публичный профиль пользователя"""
    model = Profile
    context_object_name = 'profile'
    template_name = 'profiles/public_profile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Проверяем подписан ли пользователь на данный профиль
        context['follow'] = UserFollowing.objects.filter(user_id=self.get_object().user,
                                                         following_user_id=self.request.user).exists()
        return context

    def dispatch(self, request, *args, **kwargs):
        # Если публичный профиль ведёт на нас, перенаправляем на личный профиль
        pk = self.kwargs.get('pk')
        if self.request.user.profile.pk == pk:
            return redirect('view_profile')
        self.get_object()
        return super(ProfilePublicInfo, self).get(request, *args, **kwargs)

    def get_object(self, queryset=None):
        pk = self.kwargs.get('pk')
        obj = get_object_or_404(Profile, pk=pk)
        return obj


class AddFavouriteView(LoginRequiredMixin, View):
    """Добавить в закладки"""

    def get(self, request):
        raise Http404

    def post(self, request):
        pk = request.POST.get("pk")
        news = News.objects.get(pk=pk)
        if news.favourites.filter(id=request.user.id).exists():
            news.favourites.remove(request.user)
            counter = news.favourites.count()
            result = False
        else:
            news.favourites.add(request.user)
            counter = news.favourites.count()
            result = True
        news.save()
        return JsonResponse({'result': result, 'counter': counter, })


class FavouriteListView(LoginRequiredMixin, ListView):
    """Список новостей в закладках"""
    model = News
    template_name = 'profiles/profile_favourites_sidebar.html'
    context_object_name = 'favorites_list'
    paginate_by = 5

    def get_queryset(self):
        queryset = News.objects.filter(favourites=self.request.user)
        return queryset


class FollowProfileView(LoginRequiredMixin, View):
    """Подписаться на профиль"""

    def post(self, request):
        pk = request.POST.get("pk")
        profile = Profile.objects.get(pk=pk)
        follow = UserFollowing.objects.filter(user_id=profile.user, following_user_id=self.request.user).exists()
        if follow:
            UserFollowing.objects.filter(user_id=profile.user, following_user_id=self.request.user).delete()
            result = False
        else:
            UserFollowing.objects.create(user_id=profile.user, following_user_id=self.request.user)
            result = True
        return JsonResponse({'result': result})


class ProfileNewsView(LoginRequiredMixin, ListView):
    """В профиле пользователя написанные им статьи"""
    model = Profile
    template_name = 'profiles/profile_news.html'
    context_object_name = 'profile_news'
    paginate_by = 5

    def get_profile(self):
        profile = get_object_or_404(Profile, user=self.request.user)
        return profile

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        profile = self.get_profile()
        context['profile'] = profile
        return context

    def get_queryset(self):
        profile = self.get_profile()
        queryset = profile.user.news.all()
        return queryset


class ProfileCommentsView(ProfileNewsView):
    """В профиле пользователя написанные им комментарии"""
    model = Profile
    template_name = 'profiles/profile_comments.html'
    context_object_name = 'profile_comments'
    paginate_by = 5

    def get_queryset(self):
        profile = self.get_profile()
        queryset = profile.user.comment.all()
        return queryset


class ProfileFavoritesView(ProfileNewsView):
    """В профиле пользователя его закладки"""
    model = Profile
    template_name = 'profiles/profile_favorites.html'
    context_object_name = 'profile_favorites'
    paginate_by = 5

    def get_queryset(self):
        profile = self.get_profile()
        queryset = News.objects.filter(favourites=profile.user)
        return queryset


class ProfileFollowView(ProfileNewsView):
    """В профиле пользователя подписки и подписчики"""
    model = Profile
    template_name = 'profiles/profile_follow.html'
    context_object_name = 'profile_follow'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        # Выводим информацию о пользователи
        profile = self.get_profile()

        # Списки подписок и подписчиков
        context['following_list'] = self.get_following(profile)[:6]
        context['followers_list'] = self.get_followers(profile)[:6]

        return context

    def get_following(self, profile):
        # Списки подписок
        following_list = UserFollowing.objects.filter(following_user_id=profile.user)
        return following_list

    def get_followers(self, profile):
        # Списки подписчиков
        followers_list = UserFollowing.objects.filter(user_id=profile.user)
        return followers_list


class ProfileSubscriptionsView(ProfileFollowView):
    """В профиле пользователя выводим все подписки"""
    model = Profile
    template_name = 'profiles/profile_subscriptions.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        # Выводим информацию о пользователи
        profile = self.get_profile()

        # Передаём список подписок
        context['following_list'] = self.get_following(profile)
        return context


class ProfileSubscribersView(ProfileFollowView):
    """В профиле пользователя выводим всх подписчиков"""
    model = Profile
    template_name = 'profiles/profile_subscribers.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        # Выводим информацию о пользователи
        profile = self.get_profile()

        # Передаём список подписчиков
        context['followers_list'] = self.get_followers(profile)

        # Передаём список подписок пользователя для сравнения
        context['subscriptions_list'] = self.get_user_subscriptions(profile)
        return context

    def get_user_subscriptions(self, profile):
        # Список подписок для сравнения
        following_list = []
        for i in UserFollowing.objects.filter(following_user_id=profile.user):
            following_list.append(i.user_id)
        return following_list


class PublicProfileNewsView(ProfileNewsView):
    """В публичном профиле пользователя написанные им статьи"""
    model = Profile
    template_name = 'profiles/public_profile_news.html'
    context_object_name = 'profile_news'
    paginate_by = 5

    def get_public_profile(self):
        # Выводим публичную информацию о пользователи
        pk = self.kwargs.get('pk')
        profile = get_object_or_404(Profile, pk=pk)
        return profile

    def check_follow(self, public_profile, profile):
        # Проверяем подписан ли пользователь на данный профиль
        follow = UserFollowing.objects.filter(user_id=public_profile.user,
                                              following_user_id=profile.user).exists()
        return follow

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        # Выводим публичную информацию о пользователи
        profile = self.get_profile()
        public_profile = self.get_public_profile()
        context['profile'] = public_profile
        context['follow'] = self.check_follow(public_profile=public_profile, profile=profile)
        return context

    def get_queryset(self):
        # Статьи пользователя
        profile = self.get_public_profile()
        queryset = profile.user.news.all()
        return queryset


class PublicProfileCommentsView(PublicProfileNewsView):
    """В публичном профиле пользователя написанные им комментарии"""
    model = Profile
    template_name = 'profiles/public_profile_comments.html'
    context_object_name = 'profile_comments'
    paginate_by = 5

    def get_queryset(self):
        # Комментарии пользователя
        profile = self.get_public_profile()
        queryset = profile.user.comment.all()
        return queryset


class PublicProfileFavoritesView(PublicProfileNewsView):
    """В публичном профиле пользователя написанные им комментарии"""
    model = Profile
    template_name = 'profiles/public_profile_favorites.html'
    context_object_name = 'profile_favorites'
    paginate_by = 5

    def get_queryset(self):
        # Закладки пользователя
        profile = self.get_public_profile()
        queryset = News.objects.filter(favourites=profile.user)
        return queryset


class PublicProfileFollowView(PublicProfileNewsView, ProfileFollowView):
    """В публичном профиле пользователя его подписки и подписчики"""
    model = Profile
    template_name = 'profiles/public_profile_follow.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        # Выводим публичную информацию о пользователи
        profile = self.get_profile()
        public_profile = self.get_public_profile()
        context['profile'] = public_profile
        context['follow'] = self.check_follow(public_profile=public_profile, profile=profile)

        # Списки подписок и подписчиков
        context['following_list'] = self.get_following(public_profile)[:6]
        context['followers_list'] = self.get_followers(public_profile)[:6]

        return context


class PublicProfileSubscriptionsView(PublicProfileFollowView, ProfileSubscribersView):
    template_name = 'profiles/public_profile_subscriptions.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        # Выводим публичную информацию о пользователи
        profile = self.get_profile()
        public_profile = self.get_public_profile()
        context['profile'] = public_profile
        context['follow'] = self.check_follow(public_profile=public_profile, profile=profile)

        # Передаём список подписок публичного профиля
        context['following_list'] = self.get_following(public_profile)

        # Передаём список подписок пользователя для сравнения
        context['subscriptions_list'] = self.get_user_subscriptions(profile)

        return context


class PublicProfileSubscribersView(PublicProfileSubscriptionsView):
    template_name = 'profiles/public_profile_subscribers.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        # Выводим публичную информацию о пользователи
        profile = self.get_profile()
        public_profile = self.get_public_profile()
        context['profile'] = public_profile
        context['follow'] = self.check_follow(public_profile=public_profile, profile=profile)

        # Передаём список подписчиков публичного профиля
        context['followers_list'] = self.get_followers(public_profile)

        # Передаём список подписок пользователя для сравнения
        context['subscriptions_list'] = self.get_user_subscriptions(profile)

        return context
