from django import template

from profiles.models import Profile
from profiles.services import get_avatar_webp
from utils import get_file_name

register = template.Library()


@register.simple_tag()
def get_webp_avatar(user_id):
    """Отдаём webp avatar"""
    # get current avatar name
    user = Profile.objects.get(pk=user_id)
    avatar_path = user.avatar.url
    img, img_name = get_file_name(path=avatar_path)

    avatar_webp = get_avatar_webp(user_id=user_id, img=img, img_name=img_name)

    return avatar_webp
