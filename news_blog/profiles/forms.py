from django import forms
from .models import Profile
from .validators import avatar_validation, nickname_validation


class ProfileForm(forms.ModelForm):
    """Форма для редактирования данных профиля"""

    class Meta:
        model = Profile
        fields = ['nickname', 'avatar']
        widgets = {
            'nickname': forms.TextInput(attrs={'class': 'form-edit-profile__input', 'placeholder': "Заголовок..."}),
            'avatar': forms.ClearableFileInput(attrs={'class': ''}),
        }

    def clean_avatar(self):
        avatar = self.cleaned_data['avatar']
        if avatar:
            avatar_validation(avatar=avatar)
            return avatar

    def clean_nickname(self):
        nickname = self.cleaned_data['nickname']
        if nickname:
            nickname_validation(nickname=nickname)
            return nickname
