from django.contrib import admin
from .models import Profile, UserFollowing


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    """Профиль"""
    list_display = ('user', 'nickname', 'pk')


@admin.register(UserFollowing)
class UserFollowingAdmin(admin.ModelAdmin):
    """Подписки"""
    list_display = ('id', 'user_id', 'following_user_id',)