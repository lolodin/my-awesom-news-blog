from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from profiles.services import get_path_upload_avatar


class Profile(models.Model):
    """Модель профиля пользователя"""
    user = models.OneToOneField(User, verbose_name='Пользователь', on_delete=models.CASCADE, related_name='profile',
                                primary_key=True)
    nickname = models.CharField('Псевдоним', max_length=20, null=True, blank=True)
    avatar = models.ImageField('Аватар', upload_to=get_path_upload_avatar, null=True, blank=True)

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'

    def __str__(self):
        return f'{self.user}'

    def get_all_like(self):
        news = self.user.news.all()
        comments = self.user.comment.all()
        all_like = 0
        for i in news:
            all_like += i.like
        for comment in comments:
            all_like += comment.like
        return all_like

    @property
    def get_avatar_url(self):
        if self.avatar:
            return '/media/{}'.format(self.avatar)
        else:
            return '/static/img/default.png'


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    """Создание профиля пользователя при регистрации"""
    if created:
        Profile.objects.create(user=instance, pk=instance.pk)
        instance.profile.save()


class UserFollowing(models.Model):
    user_id = models.ForeignKey(User, verbose_name='Подписки', related_name="following", on_delete=models.CASCADE)
    following_user_id = models.ForeignKey(User, verbose_name='Подписчики', related_name="followers",
                                          on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Подписка'
        verbose_name_plural = 'Подписчики'
        unique_together = ('user_id', 'following_user_id')
