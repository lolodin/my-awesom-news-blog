from PIL import Image
from django import forms


def avatar_validation(avatar):
    MIN_RESOLUTION = 100, 100
    MAX_RESOLUTION = 200, 200
    MAX_UPLOAD_SIZE = 5242880

    if avatar.size > MAX_UPLOAD_SIZE:
        message = 'Максимальый размер изображения 5 МБ'
        raise forms.ValidationError(message)

    img = Image.open(avatar)
    min_width, min_height = MIN_RESOLUTION
    max_width, max_height = MAX_RESOLUTION

    if img.width < min_width or img.height < min_height:
        message = f'Разрешение изображения меньше минимального ' \
                  f'({min_width}x{min_height})'
        raise forms.ValidationError(message)
    if img.width > max_width or img.height > max_height:
        message = f'Разрешение изображения больше максимального ' \
                  f'({max_width}x{max_height})'
        raise forms.ValidationError(message)


def nickname_validation(nickname):
    symbols = r'".,:;!_*-+()/#¤%&`\"'
    if len(set(nickname) & set(symbols)) > 0:  # выведет True:
        message = f'Запрешённые символы {symbols}'
        raise forms.ValidationError(message)