from news_blog.celery import app
from .service import send_feedback
from .models import ContactUs, StatusFeedback


@app.task
def send_new_feedback():
    """Оповещение о новых заявках обратной связи"""

    for feedback in ContactUs.objects.filter(status=StatusFeedback.objects.get(id=5)):
        send_feedback(user_email=feedback.email, user_message=feedback.comment)
        feedback.status = StatusFeedback.objects.get(id=1)
        feedback.save()


@app.task
def feedback_statistic():
    """Обновляет количество заявок по каждому статусу"""

    status_open = StatusFeedback.objects.get(id=1)
    status_cancel = StatusFeedback.objects.get(id=2)
    status_complete = StatusFeedback.objects.get(id=3)
    status_archive = StatusFeedback.objects.get(id=4)
    status_new = StatusFeedback.objects.get(id=5)

    status_open.count = 0
    status_complete.count = 0
    status_cancel.count = 0
    status_archive.count = 0
    status_new.count = 0

    for feedback in ContactUs.objects.all():
        if feedback.status.name == 'Открыта':
            status_open.count += 1
        elif feedback.status.name == 'Отменена':
            status_cancel.count += 1
        elif feedback.status.name == 'Выполнена':
            status_complete.count += 1
        elif feedback.status.name == 'Архив':
            status_archive.count += 1
        else:
            status_new.count += 1

    status_open.save()
    status_complete.save()
    status_cancel.save()
    status_archive.save()
    status_new.save()


@app.task
def send_feedback_statistic():
    """Отправляет статистику по обратной связи"""

    status_open = int(StatusFeedback.objects.get(id=1).count)
    status_cancel = int(StatusFeedback.objects.get(id=2).count)
    status_complete = int(StatusFeedback.objects.get(id=3).count)
    status_archive = int(StatusFeedback.objects.get(id=4).count)
    status_new = int(StatusFeedback.objects.get(id=5).count)
    all_feedback = int(ContactUs.objects.all().count())

    message = f'''
        Заявок открыто - {status_open};
        Заявок отменено - {status_cancel};
        Заявок выполнено - {status_complete};
        Заявок в архиве - {status_archive};
        Заявок новых - {status_new};
        
        Всего: {all_feedback}.
    '''
    email = 'myawesomnewsblog@gmail.com'
    send_feedback(user_email=email, user_message=message)


@app.task
def send_answer_feedback():
    """Отправляет ответ на заявку пользователю"""

    for feedback in ContactUs.objects.filter(status=StatusFeedback.objects.get(id=3)):
        if feedback.answer and feedback.answer.strip() != '':
            send_feedback(user_email='myawesomnewsblog@gmail.com', user_message=feedback.answer, user_email_to=feedback.email)
            feedback.status = StatusFeedback.objects.get(id=4)
            feedback.save()
