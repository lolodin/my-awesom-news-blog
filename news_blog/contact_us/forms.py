from django import forms
from .models import ContactUs


class ContactUsForm(forms.ModelForm):
    """Форма для связи с нами"""

    class Meta:
        model = ContactUs
        fields = ('name', 'email', 'comment')
