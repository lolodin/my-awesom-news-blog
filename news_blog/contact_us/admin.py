from django.contrib import admin
from .models import ContactUs, StatusFeedback


@admin.register(ContactUs)
class ContactUsAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'email', 'comment', 'created_at', 'status')
    list_filter = ('email', 'created_at')


@admin.register(StatusFeedback)
class StatusFeedbackAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'count')
