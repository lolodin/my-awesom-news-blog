from django.db import models


class StatusFeedback(models.Model):
    """Статус заявки для обратной связи"""

    name = models.CharField('Название', max_length=25)
    count = models.IntegerField('Количество', default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Статус обращения'
        verbose_name_plural = 'Статусы обращений'


class ContactUs(models.Model):
    """Заявка для обратной связи"""

    name = models.CharField('Имя', max_length=50)
    email = models.EmailField('E-mail', max_length=100)
    comment = models.TextField('Сообщение', max_length=2000)
    created_at = models.DateTimeField('Время отправки', auto_now_add=True)
    status = models.ForeignKey(StatusFeedback, verbose_name='feedback_status', on_delete=models.CASCADE, default=None,
                               null=True)
    answer = models.TextField('Ответ', default=None, null=True)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Связь с нами'
        verbose_name_plural = 'Связь с нами'
        ordering = ['-created_at']



