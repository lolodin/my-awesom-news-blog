from django.core.mail import send_mail


def send_feedback(user_email, user_message, user_email_to='myawesomnewsblog@gmail.com'):
    """Отправка письма"""

    send_mail(
        'Feedback [MY AWESOME NES BLOG]',
        f'''
            {user_message}
            
            From: {user_email}
        ''',
        'myawesomnewsblog@gmail.com',
        [user_email_to],
        fail_silently=False,
    )
