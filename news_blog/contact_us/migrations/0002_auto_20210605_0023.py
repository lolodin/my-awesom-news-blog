# Generated by Django 3.1.7 on 2021-06-04 21:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contact_us', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='StatusFeedback',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=25, verbose_name='Название')),
                ('count', models.IntegerField(default=0, verbose_name='Количество')),
            ],
            options={
                'verbose_name': 'Статус обращения',
                'verbose_name_plural': 'Статусы обращений',
            },
        ),
        migrations.AddField(
            model_name='contactus',
            name='status',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='contact_us.statusfeedback', verbose_name='feedback_status'),
        ),
    ]
