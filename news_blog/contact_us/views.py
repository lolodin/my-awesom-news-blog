from django.http import JsonResponse
from django.shortcuts import redirect
from django.views import View
from .models import StatusFeedback
from .forms import ContactUsForm


class ContactUsView(View):
    """Отправка формы обратной связи"""

    def post(self, request):

        # if request.method == 'POST':
        #     form = ContactUsForm(request.POST)
        #     if form.is_valid():
        #         form.save()
        #     return redirect('/')

        if request.is_ajax():
            form = ContactUsForm(request.POST)
            form.instance.status = StatusFeedback.objects.get(id=5)

            if form.is_valid():
                data = {
                    'title': form.cleaned_data['title'],
                    'description': form.cleaned_data['description'],
                    'category': form.cleaned_data['category'],
                    'image': form.cleaned_data['image'],
                }

                form.save()

                return JsonResponse({'success': data})
            else:
                return JsonResponse({'errors': form.errors})
        return redirect('/')
